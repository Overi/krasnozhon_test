﻿using System;
using System.IO;
using System.Threading;

namespace DirectoryExplorer
{
    class DirectoryScanner
    {
        Thread thread;

        string directoryPath;
        ScanResultsContainer resultsContainer;

        public DirectoryScanner(string directoryPath, ScanResultsContainer resultsContainer)
        {
            this.directoryPath = directoryPath;
            this.resultsContainer = resultsContainer;
        }

        public void Run()
        {
            thread = new Thread(new ThreadStart(StartScan));
            thread.Start();
        }

        private void StartScan()
        {
            resultsContainer.ScanCompleted = false;

            int indent = 0;
            Scan(directoryPath, ref indent);

            resultsContainer.ScanCompleted = true;
        }

        private void Scan(string directory, ref int indent)
        {
            try
            {
                foreach (string d in Directory.GetDirectories(directory))
                {
                    var directoryInfo = new DirectoryInfo(d);
                    var result = new ScanResult(directoryInfo, indent);
                    resultsContainer.AddResult(result);

                    ++indent;
                    foreach (string f in Directory.GetFiles(d))
                    {
                        var fileInfo = new FileInfo(f);
                        result = new ScanResult(fileInfo, indent);
                        resultsContainer.AddResult(result);
                    }
                    --indent;

                    if (!string.IsNullOrEmpty(d))
                    {
                        ++indent;
                        Scan(d, ref indent);
                    }
                }
            }
            catch (System.Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            --indent;
        }
    }
}
