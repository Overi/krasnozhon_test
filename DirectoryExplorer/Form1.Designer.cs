﻿namespace DirectoryExplorer
{
    partial class DirectoryExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GoButton = new System.Windows.Forms.Button();
            this.treeView = new System.Windows.Forms.TreeView();
            this.scanTextBox = new System.Windows.Forms.TextBox();
            this.scanPathGroupBox = new System.Windows.Forms.GroupBox();
            this.scanBrowseButton = new System.Windows.Forms.Button();
            this.outputPathGroupBox = new System.Windows.Forms.GroupBox();
            this.outputBrowseButton = new System.Windows.Forms.Button();
            this.outputPathTextBox = new System.Windows.Forms.TextBox();
            this.scanPathGroupBox.SuspendLayout();
            this.outputPathGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(547, 455);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(331, 65);
            this.GoButton.TabIndex = 0;
            this.GoButton.Text = "SCAN";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButtonClick);
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(12, 12);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(516, 508);
            this.treeView.TabIndex = 2;
            // 
            // scanTextBox
            // 
            this.scanTextBox.Location = new System.Drawing.Point(6, 19);
            this.scanTextBox.Name = "scanTextBox";
            this.scanTextBox.Size = new System.Drawing.Size(198, 20);
            this.scanTextBox.TabIndex = 3;
            this.scanTextBox.Text = "F:\\\\Download\\\\heroes5 utilities";
            // 
            // scanPathGroupBox
            // 
            this.scanPathGroupBox.Controls.Add(this.scanBrowseButton);
            this.scanPathGroupBox.Controls.Add(this.scanTextBox);
            this.scanPathGroupBox.Location = new System.Drawing.Point(553, 302);
            this.scanPathGroupBox.Name = "scanPathGroupBox";
            this.scanPathGroupBox.Size = new System.Drawing.Size(331, 54);
            this.scanPathGroupBox.TabIndex = 4;
            this.scanPathGroupBox.TabStop = false;
            this.scanPathGroupBox.Text = "Scan Path";
            // 
            // scanBrowseButton
            // 
            this.scanBrowseButton.Location = new System.Drawing.Point(228, 16);
            this.scanBrowseButton.Name = "scanBrowseButton";
            this.scanBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.scanBrowseButton.TabIndex = 5;
            this.scanBrowseButton.Text = "Browse";
            this.scanBrowseButton.UseVisualStyleBackColor = true;
            this.scanBrowseButton.Click += new System.EventHandler(this.scanBrowseButton_Click);
            // 
            // outputPathGroupBox
            // 
            this.outputPathGroupBox.Controls.Add(this.outputBrowseButton);
            this.outputPathGroupBox.Controls.Add(this.outputPathTextBox);
            this.outputPathGroupBox.Location = new System.Drawing.Point(553, 373);
            this.outputPathGroupBox.Name = "outputPathGroupBox";
            this.outputPathGroupBox.Size = new System.Drawing.Size(331, 54);
            this.outputPathGroupBox.TabIndex = 6;
            this.outputPathGroupBox.TabStop = false;
            this.outputPathGroupBox.Text = "Output Path";
            // 
            // outputBrowseButton
            // 
            this.outputBrowseButton.Location = new System.Drawing.Point(228, 16);
            this.outputBrowseButton.Name = "outputBrowseButton";
            this.outputBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.outputBrowseButton.TabIndex = 5;
            this.outputBrowseButton.Text = "Browse";
            this.outputBrowseButton.UseVisualStyleBackColor = true;
            this.outputBrowseButton.Click += new System.EventHandler(this.outputBrowseButton_Click);
            // 
            // outputPathTextBox
            // 
            this.outputPathTextBox.Location = new System.Drawing.Point(6, 19);
            this.outputPathTextBox.Name = "outputPathTextBox";
            this.outputPathTextBox.Size = new System.Drawing.Size(198, 20);
            this.outputPathTextBox.TabIndex = 3;
            this.outputPathTextBox.Text = "F:\\\\test_files.xml";
            // 
            // DirectoryExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 547);
            this.Controls.Add(this.outputPathGroupBox);
            this.Controls.Add(this.scanPathGroupBox);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.GoButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "DirectoryExplorer";
            this.Text = "DirectoryExplorer";
            this.scanPathGroupBox.ResumeLayout(false);
            this.scanPathGroupBox.PerformLayout();
            this.outputPathGroupBox.ResumeLayout(false);
            this.outputPathGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.TextBox scanTextBox;
        private System.Windows.Forms.GroupBox scanPathGroupBox;
        private System.Windows.Forms.Button scanBrowseButton;
        private System.Windows.Forms.GroupBox outputPathGroupBox;
        private System.Windows.Forms.Button outputBrowseButton;
        private System.Windows.Forms.TextBox outputPathTextBox;
    }
}

