﻿using System.Xml;

namespace DirectoryExplorer
{
    class XmlExporter : ResultsExporter
    {
        string outputPath;

        XmlDocument document;
        XmlNode currentParent;

        int currentParentIndent;

        public XmlExporter(string outputPath, ScanResultsContainer resultsContainer)
        {
            this.outputPath = outputPath;
            this.resultsContainer = resultsContainer;
        }

        protected override void Export()
        {
            int index = 0;
            bool exportCompleted = false;

            CreateDocument();
            AddHeader();

            while (!exportCompleted)
            {
                ScanResult scanResult = resultsContainer.GetScanResult(index);

                if (scanResult != null)
                {
                    ++index;

                    AddNode(scanResult);
                }

                exportCompleted = resultsContainer.ScanCompleted && scanResult == null;
            }

            WriteDocumentToFile();
        }

        void CreateDocument()
        {
            document = new XmlDocument();
            currentParent = document;
        }

        void AddHeader()
        {
            XmlNode docNode = document.CreateXmlDeclaration("1.0", "UTF-8", null);
            document.AppendChild(docNode);

            XmlNode scanResultsNode = document.CreateElement("ScanResults");
            document.AppendChild(scanResultsNode);
            currentParent = scanResultsNode;
            currentParentIndent = 0;
        }

        void AddNode(ScanResult scanResult)
        {
            RefreshCurrentParent(scanResult.GetIndent());

            XmlNode infoNode = document.CreateElement("FileSystemInfo");

            AppendAttribute(infoNode, "Name", scanResult.GetName());
            AppendAttribute(infoNode, "CreationDate", scanResult.GetCreationDate());
            AppendAttribute(infoNode, "LastAccessDate", scanResult.GetLastAccessDate());
            AppendAttribute(infoNode, "Attributes", scanResult.GetAttributes());
            AppendAttribute(infoNode, "Size", scanResult.GetSizeString());
            AppendAttribute(infoNode, "Owner", scanResult.GetOwner());
            AppendAttribute(infoNode, "AccessRulesForCurrentUser", scanResult.GetAccessRulesForCurrentUser());

            currentParent.AppendChild(infoNode);
        }

        void AppendAttribute(XmlNode parent, string key, string value)
        {
            var attribute = document.CreateAttribute(key);
            attribute.Value = value;
            parent.Attributes.Append(attribute);
        }

        protected void RefreshCurrentParent(int scanResultIndent)
        {
            int indentDiff = currentParentIndent - scanResultIndent;

            //парент уже правильный
            if (indentDiff == 0)
            {
                return;
            }

            if (indentDiff > 0)
            {
                for (int i = 0; i < indentDiff; ++i)
                {
                    currentParent = currentParent.ParentNode;
                }
            }
            else if (indentDiff < 0)
            {
                for (int i = 0; i < -indentDiff; ++i)
                {
                    currentParent = currentParent.LastChild;
                }
            }
            currentParentIndent = scanResultIndent;
        }

        void WriteDocumentToFile()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(outputPath, settings))
            {
                document.WriteTo(writer);
                //refactor - dont use XmlDocument, export directly with writer
            }
        }
    }
}
