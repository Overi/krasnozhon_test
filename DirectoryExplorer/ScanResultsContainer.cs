﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DirectoryExplorer
{
    class ScanResultsContainer
    {
        List<ScanResult> results = new List<ScanResult>();

        bool scanCompleted = false;
        public bool ScanCompleted
        {
            get { return scanCompleted; }
            set { scanCompleted = value; }
        }

        public ScanResult GetScanResult(int index)
        {
            if (results == null)
            {
                return null;
            }

            if (results.Count <= index)
            {
                return null;
            }

            return results[index];
        }

        public void AddResult(ScanResult info)
        {
            results.Add(info);
        }
    }
}
