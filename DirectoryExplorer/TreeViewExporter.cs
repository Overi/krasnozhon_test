﻿using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DirectoryExplorer
{
    class TreeViewExporter : ResultsExporter
    {
        TreeView treeView;

        TreeNode currentParent;
        int currentParentIndent;

        delegate void AddNodeCallback(TreeNode node, TreeNode parent);
        delegate void ClearTreeViewCallback();

        public TreeViewExporter(TreeView treeView, ScanResultsContainer resultsContainer)
        {
            this.treeView = treeView;
            this.resultsContainer = resultsContainer;
        }

        protected override void Export()
        {
            int index = 0;
            bool exportCompleted = false;

            ClearViewSafe();
            AddHeader();

            while (!exportCompleted)
            {
                ScanResult scanResult = resultsContainer.GetScanResult(index);

                if (scanResult != null)
                {
                    ++index;

                    AddNode(scanResult);
                }

                exportCompleted = resultsContainer.ScanCompleted && scanResult == null;
            }
        }

        void ClearViewSafe()
        {
            if (treeView.InvokeRequired)
            {
                var form = treeView.FindForm();
                var d = new ClearTreeViewCallback(ClearViewSafe);
                form.Invoke(d);
            }
            else
            {
                treeView.Nodes.Clear();
            }
        }

        void AddHeader()
        {
            var node = new TreeNode("root");

            AddNodeSafe(node, null);

            currentParentIndent = 0;
            currentParent = node;
        }

        private void AddNode(ScanResult scanResult)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(scanResult.GetName());
            stringBuilder.AppendFormat(" {0}", scanResult.GetCreationDate());
            stringBuilder.AppendFormat(" {0}", scanResult.GetLastAccessDate());
            stringBuilder.AppendFormat(" {0}", scanResult.GetAttributes());
            stringBuilder.AppendFormat(" {0}", scanResult.GetSizeString());
            stringBuilder.AppendFormat(" {0}", scanResult.GetOwner());
            stringBuilder.AppendFormat(" {0}", scanResult.GetAccessRulesForCurrentUser());

            var node = new TreeNode(stringBuilder.ToString());
            RefreshCurrentParent(scanResult.GetIndent());
            AddNodeSafe(node, currentParent);
        }

        private void AddNodeSafe(TreeNode node, TreeNode parent)
        {
            if (treeView.InvokeRequired)
            {
                var form = treeView.FindForm();
                var d = new AddNodeCallback(AddNodeSafe);
                form.Invoke(d, new object[] { node, parent });
            }
            else
            {
                treeView.BeginUpdate();

                if (parent != null)
                {
                    parent.Nodes.Add(node);
                }
                else
                {
                    treeView.Nodes.Add(node);
                }

                treeView.EndUpdate();
                treeView.ExpandAll();
            }
        }

        protected void RefreshCurrentParent(int scanResultIndent)
        {
            int indentDiff = currentParentIndent - scanResultIndent;

            if (indentDiff == 0)
            {
                return;
            }

            if (indentDiff > 0)
            {
                for (int i = 0; i < indentDiff; ++i)
                {
                    currentParent = currentParent.Parent;
                }
            }
            else if (indentDiff < 0)
            {
                for (int i = 0; i < -indentDiff; ++i)
                {
                    currentParent = currentParent.LastNode;
                }
            }

            currentParentIndent = scanResultIndent;
        }
    }
}
