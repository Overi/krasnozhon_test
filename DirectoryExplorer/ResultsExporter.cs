﻿using System.Threading;

namespace DirectoryExplorer
{
    abstract class ResultsExporter
    {
        protected Thread thread;
        protected ScanResultsContainer resultsContainer;

        public virtual void Run()
        {
            thread = new Thread(new ThreadStart(Export));
            thread.Start();
        }

        protected abstract void Export();

    }
}
