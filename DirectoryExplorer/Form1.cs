﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DirectoryExplorer
{
    public partial class DirectoryExplorer : Form
    {
        public DirectoryExplorer()
        {
            InitializeComponent();
        }

        private void GoButtonClick(object sender, EventArgs e)
        {
            Run();
        }

        private void scanBrowseButton_Click(object sender, EventArgs e)
        {
            SelectScanPath();
        }

        private void outputBrowseButton_Click(object sender, EventArgs e)
        {
            SelectOutputPath();
        }


        void Run()
        {
            var outputPath = "F:\\test_files.xml";
            var scanPath = scanTextBox.Text;

            if (string.IsNullOrEmpty(scanPath))
            {
                MessageBox.Show("Error", "Scan Path is empty!\nSet path via 'Browse' button or textbox.");
                return;
            }

            if (string.IsNullOrEmpty(outputPath))
            {
                MessageBox.Show("Error", "Output Path is empty!\nSet path via 'Browse' button or textbox.");
                return;
            }

            var results = new ScanResultsContainer();

            var scanner = new DirectoryScanner(scanPath, results);
            scanner.Run();

            var exporters = new List<ResultsExporter>();
            exporters.Add(new ConsoleExporter(results));
            exporters.Add(new XmlExporter(outputPath, results));
            exporters.Add(new TreeViewExporter(treeView, results));

            foreach (ResultsExporter exporter in exporters)
            {
                exporter.Run();
            }

            //clear results somehow???
        }

        private void SelectScanPath()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                DialogResult result = dialog.ShowDialog();
                var path = dialog.SelectedPath;
                if (result == DialogResult.OK && !string.IsNullOrEmpty(path))
                {
                    scanTextBox.Text = path;
                }
            }
        }

        private void SelectOutputPath()
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
                dialog.FilterIndex = 1;

                DialogResult result = dialog.ShowDialog();
                var path = dialog.FileName;
                if (result == DialogResult.OK && !string.IsNullOrEmpty(path))
                {
                    outputPathTextBox.Text = path;
                }
            }
        }

    }
}
