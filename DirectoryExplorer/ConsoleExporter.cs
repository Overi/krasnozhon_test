﻿using System;
using System.Text;

namespace DirectoryExplorer
{
    class ConsoleExporter : ResultsExporter
    {
        public ConsoleExporter(ScanResultsContainer resultsContainer)
        {
            this.resultsContainer = resultsContainer;
        }

        protected override void Export()
        {
            int index = 0;
            bool exportCompleted = false;

            while (!exportCompleted)
            {
                ScanResult result = resultsContainer.GetScanResult(index);

                if (result != null)
                {
                    ++index;

                    Console.WriteLine(CreateInfoString(result));
                }

                exportCompleted = resultsContainer.ScanCompleted && result == null;
            }
        }

        static string CreateInfoString(ScanResult scanResult)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine(scanResult.GetName());
            stringBuilder.AppendLine(scanResult.GetCreationDate());
            stringBuilder.AppendLine(scanResult.GetLastAccessDate());
            stringBuilder.AppendLine(scanResult.GetAttributes());
            stringBuilder.AppendLine(scanResult.GetSizeString());
            stringBuilder.AppendLine(scanResult.GetOwner());
            stringBuilder.AppendLine(scanResult.GetAccessRulesForCurrentUser());

            return stringBuilder.ToString();
        }

    }
}
