﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace DirectoryExplorer
{
    class ScanResult
    {
        private FileSystemInfo fileSystemInfo;
        private int indent;

        public ScanResult(FileSystemInfo fileSystemInfo, int indent)
        {
            this.fileSystemInfo = fileSystemInfo;
            this.indent = indent;
        }

        public int GetIndent()
        {
            return indent;
        }

        public string GetName()
        {
            return fileSystemInfo.FullName;
        }

        public string GetCreationDate()
        {
            return fileSystemInfo.CreationTime.ToString("dd/MM/yyyy");
        }

        public string GetLastAccessDate()
        {
            return fileSystemInfo.LastAccessTime.ToString("dd/MM/yyyy");
        }

        public string GetAttributes()
        {
            return fileSystemInfo.Attributes.ToString();
        }

        public string GetOwner()
        {
            FileSecurity fileSecurity = File.GetAccessControl(fileSystemInfo.FullName);
            IdentityReference identityReference = fileSecurity.GetOwner(typeof(SecurityIdentifier));

            return identityReference.Value;
        }

        public string GetAccessRulesForCurrentUser()
        {
            //TODO: implement
            return "";
        }

        public long GetSizeInBytes()
        {
            return GetSizeInBytes(fileSystemInfo);
        }

        public string GetSizeString()
        {
            long sizeInBytes = GetSizeInBytes(fileSystemInfo);

            if (sizeInBytes == 0)
            {
                return "";
            }

            double size = sizeInBytes;
            string units = "Bytes";

            if (size > 1024)
            {
                size /= 1024;
                units = "KB";
            }
            if (size > 1024)
            {
                size /= 1024;
                units = "MB";
            }
            if (size > 1024)
            {
                size /= 1024;
                units = "GB";
            }

            return string.Format("{0:0.00}{1}", size, units);
        }

        static long GetSizeInBytes(FileSystemInfo info)
        {
            //TODO: refactor - add extension to FileSystemInfo
            var fileInfo = info as FileInfo;
            if (fileInfo != null)
            {
                return GetSizeInBytes(fileInfo);
            }

            var directoryInfo = info as DirectoryInfo;
            if (directoryInfo != null)
            {
                return GetSizeInBytes(directoryInfo);
            }

            return 0;
        }

        static long GetSizeInBytes(FileInfo info)
        {
            return info.Length;
        }

        static long GetSizeInBytes(DirectoryInfo info)
        {
            //TODO: implement
            return 0;
        }
    }
}
