#pragma once

#include <string>

template <class TData> class CArray;

class StringTest
{
public:

  static void StartTest();

private:

  static void PushBackRandomEntries(
      CArray<std::string>& cArray
    );
  static void SortAscending(
      CArray<std::string>& cArray
    );
  static void EraseSome(
      CArray<std::string>& cArray
    );
  static void InsertRandomEntries(
      CArray<std::string>& cArray
    );
  static void ClearContainer(
      CArray<std::string>& cArray
    );

  static std::string GenerateRandomWord();
};