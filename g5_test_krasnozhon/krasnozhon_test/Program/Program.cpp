// Program.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "CArray.h"
#include "IntTest.h"
#include "StringTest.h"

int _tmain(
    int argc,
    _TCHAR* argv[]
  )
{
  IntTest::StartTest();
  StringTest::StartTest();

  std::getchar();

  return 0;
}