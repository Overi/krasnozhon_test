#include "IntTest.h"

#include "CArray.h"

#include <ctime>

void IntTest::StartTest()
{
  srand(time(NULL));
  CArray<int> arr;

  PushBackRandomEntries(arr);
  SortAscending(arr);
  EraseSome(arr);
  InsertRandomEntries(arr);
  ClearContainer(arr);
}

void IntTest::PushBackRandomEntries(
    CArray<int>& _cArray
  )
{
  for (int i = 0; i < 20; ++i)
  {
    _cArray.push_back(std::rand() % 101);
  }

  _cArray.print();
}

void IntTest::SortAscending(
    CArray<int>& _cArray
  )
{
  for (int i = 0, size = _cArray.size(); i < size; ++i)
  {
    for (int k = i + 1; k < size; ++k)
    {
      if (_cArray[i] > _cArray[k])
      {
        auto t = _cArray[i];
        _cArray[i] = _cArray[k];
        _cArray[k] = t;
      }
    }
  }

  _cArray.print();
}

void IntTest::EraseSome(
    CArray<int>& _cArray
  )
{
  int size = _cArray.size();
  int lastEvenIndex = size - size % 2 - 1;

  for (int i = lastEvenIndex; i >= 0; i -= 2)
  {
    _cArray.erase(i);
  }

  _cArray.print();
}

void IntTest::InsertRandomEntries(
    CArray<int>& _cArray
  )
{
  for (int i = 0; i < 10; ++i)
  {
    int number = std::rand() % 101;
    int index = std::rand() % _cArray.size();

    _cArray.insert(index, number);
  }

  _cArray.print();
}

void IntTest::ClearContainer(
    CArray<int>& _cArray
  )
{
  _cArray.clear();
  _cArray.print();
}