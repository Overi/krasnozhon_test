#include "StringTest.h"

#include "CArray.h"

#include <algorithm>
#include <ctime>

void StringTest::StartTest()
{
  srand(time(NULL));
  CArray<std::string> arr;

  PushBackRandomEntries(arr);
  SortAscending(arr);
  EraseSome(arr);
  InsertRandomEntries(arr);
  ClearContainer(arr);
}

void StringTest::PushBackRandomEntries(
    CArray<std::string>& _cArray
  )
{
  for (int i = 0; i < 15; ++i)
  {
    _cArray.push_back(GenerateRandomWord());
  }

  _cArray.print();
}

void StringTest::SortAscending(
    CArray<std::string>& _cArray
  )
{
  for (int i = 0, size = _cArray.size(); i < size; ++i)
  {
    for (int k = i + 1; k < size; ++k)
    {
      if (_cArray[i] > _cArray[k])
      {
        auto t = _cArray[i];
        _cArray[i] = _cArray[k];
        _cArray[k] = t;
      }
    }
  }

  _cArray.print();
}

void StringTest::EraseSome(
    CArray<std::string>& _cArray
  )
{
  std::string searchPattern = "abcde";

  for (int i = _cArray.size() - 1; i >= 0; --i)
  {
    if (_cArray[i].find_first_of(searchPattern) != std::string::npos)
    {
      _cArray.erase(i);
    }
  }

  _cArray.print();
}

void StringTest::InsertRandomEntries(
    CArray<std::string>& _cArray
  )
{
  for (int i = 0; i < 3; ++i)
  {
    int index = std::rand() % _cArray.size();

    _cArray.insert(index, GenerateRandomWord());
  }

  _cArray.print();
}

void StringTest::ClearContainer(
    CArray<std::string>& _cArray
  )
{
  _cArray.clear();
  _cArray.print();
}

std::string StringTest::GenerateRandomWord()
{
  static const int length = 4;

  auto randchar = []() -> char
  {
    const char charset[] = "abcdefghijklmnopqrstuvwxyz";
    const size_t max_index = (sizeof(charset) - 1);
    return charset[rand() % max_index];
  };

  std::string word(length, 0);
  std::generate_n(word.begin(), length, randchar);

  return word;
}