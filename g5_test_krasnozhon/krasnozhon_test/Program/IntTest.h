#pragma once

template <typename TData> class CArray;

class IntTest
{
public:

  static void StartTest();

private:

  static void PushBackRandomEntries(
      CArray<int>& cArray
    );
  static void SortAscending(
      CArray<int>& cArray
    );
  static void EraseSome(
      CArray<int>& cArray
    );
  static void InsertRandomEntries(
      CArray<int>& cArray
    );
  static void ClearContainer(
      CArray<int>& cArray
    );
};