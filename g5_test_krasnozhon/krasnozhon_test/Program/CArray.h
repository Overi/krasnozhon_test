#pragma once

#include <stdexcept>
#include <iostream>

template <typename TData>
class CArray
{
public: // Interface

  // ����������� �� ���������
  CArray()
  {
    Capacity = DefaultCapacity;
    Size = 0;
    arr = new TData[Capacity];
  }

  // ���������� �����������
  CArray(
      const CArray & _array
    )
  {
    Capacity = _array.Capacity;
    Size = _array.Size;
    arr = new TData[Capacity];

    std::copy(_array.arr, _array.arr + _array.Size, arr);
  }

  // ����������
  ~CArray()
  {
    delete[] arr;
  }

  // �������� ������� � ����� �������
  void push_back(
      const TData & _value
    )
  {
    expandIfNeeded();

    arr[Size] = _value;
    Size++;
  }

  // �������� ������� � ������ �� ��������� �������
  void insert(
      unsigned int _index,
      const TData & _value
    )
  {
    if (_index > Size)
    {
      throw new std::out_of_range("CArray::insert 'Index of out range.'");
    }

    expandIfNeeded();

    for (unsigned int i = Size; i > _index; --i)
    {
      arr[i] = arr[i - 1];
    }
    arr[_index] = _value;

    Size++;
  }

  // ������� ������� ������� �� ��������� �������
  void erase(
      unsigned int _index
    )
  {
    if (_index >= Size)
    {
      throw new std::out_of_range("CArray::erase 'Index of out range.'");
    }

    for (unsigned int i = _index; i < Size - 1; ++i)
    {
      arr[i] = arr[i + 1];
    }

    Size--;
  }

  // �������� ������
  void clear()
  {
    Size = 0;
  }

  // �������� ������ �������
  unsigned int size() const
  {
    return Size;
  }

  // �������� ������� ������� �� ��������� �������
  TData & operator[](
      unsigned int _index
    )
  {
    if (_index >= Size)
    {
      throw new std::out_of_range("Index of out range.");
    }
    return arr[_index];
  }

  void print()
  {
    std::cout << std::endl;
    for (unsigned int i = 0; i < Size; ++i)
    {
      std::cout << arr[i] << ' ';
    }
    std::cout << std::endl;
  }

protected:

  void expandIfNeeded()
  {
    if (Size == Capacity)
    {
      expand();
    }
  }

  void expand()
  {
    Capacity *= 2;

    auto newArray = new TData[Capacity];
    std::copy(arr, arr + Size, newArray);

    delete[] arr;
    arr = newArray;
  }

protected: // Attributes

  TData* arr;

  unsigned int Size;
  unsigned int Capacity;

private:

  static const int DefaultCapacity = 32;
};